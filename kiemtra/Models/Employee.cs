﻿using System;
using System.Collections.Generic;

namespace kiemtra.Models;

public partial class Employee
{
    public int EmployeeId { get; set; }

    public string FirstName { get; set; } = null!;

    public string LastName { get; set; } = null!;

    public string ContactAddress { get; set; } = null!;

    public string Username { get; set; } = null!;

    public DateOnly? DateOfBirth { get; set; }

    public string Email { get; set; } = null!;

    public virtual ICollection<Transaction> Transactions { get; set; } = new List<Transaction>();
}
