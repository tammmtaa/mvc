﻿using System;
using System.Collections.Generic;

namespace kiemtra.Models;

public partial class Transaction
{
    public int TransactionId { get; set; }

    public int? EmployeeId { get; set; }

    public int? CustomerId { get; set; }

    public DateTime TransactionDate { get; set; }

    public decimal? Amount { get; set; }

    public string? Description { get; set; }

    public virtual Customer? Customer { get; set; }

    public virtual Employee? Employee { get; set; }
}
