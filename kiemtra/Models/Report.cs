﻿using System;
using System.Collections.Generic;

namespace kiemtra.Models;

public partial class Report
{
    public int ReportId { get; set; }

    public string ReportName { get; set; } = null!;

    public DateTime ReportDate { get; set; }

    public string? Description { get; set; }
}
