﻿using System;
using System.Collections.Generic;

namespace kiemtra.Models;

public partial class Log
{
    public int LogId { get; set; }

    public DateTime LoginDate { get; set; }

    public TimeOnly LoginTime { get; set; }

    public string Username { get; set; } = null!;

    public string? Description { get; set; }

    public virtual Customer UsernameNavigation { get; set; } = null!;
}
